using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject _prefab;

    void Start() {
        StartCoroutine(SpawnSuitHector());
    }

    IEnumerator SpawnSuitHector()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(10, 40));
            
            GameObject suitHector = Instantiate(_prefab, this.transform);

            suitHector.SetActive(true);

            Rigidbody2D rb = suitHector.GetComponent<Rigidbody2D>();

            rb.AddForce(new Vector2(Random.Range(100, 400), 60));
        }
    }
}
