using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Color _original;
    private TextMeshProUGUI _text;

    void Awake() {
        _text = transform.Find("Text").GetComponent<TextMeshProUGUI>();

        _original = _text.color;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _text.color = Color.green;
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _text.color = _original;
    }
}
