using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveJobDisplay : MonoBehaviour
{
    [SerializeField]
    private GameData _gameData;
    private TMPro.TextMeshProUGUI _text;
    
    // Start is called before the first frame update
    void Awake()
    {
        _text = this.GetComponent<TMPro.TextMeshProUGUI>();
    }

    public void OnUIUpdate() {
       _text.SetText(GameData.FormatLong(_gameData.calculatePassiveGeneration()) + "/second");
    }
}
