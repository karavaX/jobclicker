using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StorePassiveGenerator : MonoBehaviour
{
    private Image _image;
    private TextMeshProUGUI _name;
    private TextMeshProUGUI _description;
    private TextMeshProUGUI _cost;

    private Button _button;

    [System.NonSerialized]
    public bool componentError;

    public Image Image { get => _image; set => _image = value; }
    public TextMeshProUGUI Name { get => _name; set => _name = value; }
    public TextMeshProUGUI Description { get => _description; set => _description = value; }
    public TextMeshProUGUI Cost { get => _cost; set => _cost = value; }
    public Button Button { get => _button; set => _button = value; }

    void Awake()
    {
        // Get all the essential components to modify
        _image = this.transform.Find("Image").GetComponent<Image>();
        _name = this.transform.Find("Name").GetComponent<TextMeshProUGUI>();
        _description = this.transform.Find("Description").GetComponent<TextMeshProUGUI>();
        _cost = this.transform.Find("Buy/Cost").GetComponent<TextMeshProUGUI>();
        _button = this.transform.Find("Buy").GetComponent<Button>();

        // All components must be present
        componentError = _image == null || _name == null || _description == null || _cost == null;

        if (componentError) Debug.LogError("Bad naming, not found children or components");
    }

    public void Init(PassiveGenerator passiveGenerator) {
        if (!componentError) {
            _image.sprite = passiveGenerator.Image;
            _name.SetText(passiveGenerator.Tag);
            _description.SetText(passiveGenerator.Description);
            _cost.SetText(GameData.FormatLong(passiveGenerator.Price) + " Jobs");
        }
    }
}
