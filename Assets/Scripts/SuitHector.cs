using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Scripting;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class SuitHector : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset _inputAsset;
    private InputActionAsset _input;
    private InputAction _clickDown;
    private InputAction _pointer;

    [SerializeField]
    private LayerMask _clickLayer;

    private Transform _transform;

    private AudioSource _sound;

    void Awake()
    {
        _input = Instantiate(_inputAsset);

        _clickDown = _input.FindActionMap("Clicker").FindAction("MouseDown");
        _pointer = _input.FindActionMap("Clicker").FindAction("Pointer");

        _transform = GetComponent<Transform>();

        _sound = _transform.Find("Sound").GetComponent<AudioSource>();
    }

    void OnEnable()
    {
        _input.Enable();
        _clickDown.performed += OnClickDown;
    }

    void OnDisable()
    {
        _input.Disable();
        _clickDown.performed -= OnClickDown;
    }

    private void OnClickDown(InputAction.CallbackContext context)
    {
        Vector2 pointerPosition = _pointer.ReadValue<Vector2>();

        Ray ray = Camera.main.ScreenPointToRay(pointerPosition);

        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity, _clickLayer);

        if (hit.rigidbody != null)
        {
            GameManager.instance.SuitHectorGeneration();

            if (_sound != null) _sound.Play();

            Destroy(this.gameObject);
        }
    }

}
