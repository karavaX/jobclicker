using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PassiveGenerator : ScriptableObject
{
    [SerializeField]
    private string _tag;

    [SerializeField]
    private Sprite _image;

    [SerializeField]
    private string _description;

    [SerializeField]
    private long _price;

    [SerializeField]
    private long _production;

    [SerializeField]
    private long _quantity = 0;

    public string Tag { get => _tag; }
    public Sprite Image { get => _image; }
    public string Description { get => _description; }
    public long Price { get => _price; }
    public long Production { get => _production; }
    public long Quantity { get => _quantity; set => _quantity = value; }

    public void Reset() {
        _quantity = 0;
    }
}
