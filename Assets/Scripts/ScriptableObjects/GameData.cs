using System;
using System.Globalization;
using System.Numerics;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu]
public class GameData : ScriptableObject
{

    [Header("General")]
    [SerializeField]
    private long _jobs;

    [SerializeField]
    private float _playTime;

    [SerializeField]
    private int _passiveWaitTime = 1;

    [SerializeField]
    private double _passiveMultiplier = 1;

    [SerializeField]
    private double _activeMultiplier = 0.1;

    [Header("Passive generators")]
    [SerializeField]
    private PassiveGenerator[] _passiveGenerators;

    public long Jobs { get => _jobs; set => _jobs = value; }
    public float PlayTime { get => _playTime; set => _playTime = value; }
    public int PassiveWaitTime { get => _passiveWaitTime; }
    public double PassiveMultiplier { get => _passiveMultiplier; }
    public double ActiveMultiplier { get => _activeMultiplier; }
    public PassiveGenerator[] PassiveGenerators { get => _passiveGenerators; }

    public void activeGeneration()
    {
        _jobs += calculateActiveGeneration();

        CheckLong();
    }

    public void passiveGeneration()
    {
        _jobs += calculatePassiveGeneration();

        CheckLong();
    }

    public void SuitHectorGeneration() {
        _jobs += Convert.ToInt64(Random.Range(_jobs * 0.5f, _jobs));

        CheckLong();
    }

    private void CheckLong()
    {
        if (this._jobs < 0)
        {
            GameManager.instance.ChangeScene("Reincarnate");
        }
    }

    public void Reincarnate()
    {
        _jobs = 0;
        _passiveMultiplier += 0.1;
        _playTime = 0;

        foreach (PassiveGenerator passiveGenerator in _passiveGenerators)
        {
            passiveGenerator.Reset();
        }

        GameManager.instance.ChangeScene("clicker");
    }

    public long calculateActiveGeneration()
    {
        return Convert.ToInt64(this.calculatePassiveGeneration() * this._activeMultiplier) + 1;
    }

    public long calculatePassiveGeneration()
    {
        long totalProduction = 0;

        foreach (PassiveGenerator passiveGenerator in this._passiveGenerators)
        {
            totalProduction += passiveGenerator.Production * passiveGenerator.Quantity;
        }

        return Convert.ToInt64(totalProduction * _passiveMultiplier);
    }

    public void BuyPassiveGenerator(string tag)
    {
        PassiveGenerator generator = GetPassiveGeneratorByTag(tag);

        if (generator != null && hasEnough_jobs(generator.Price))
        {
            _jobs -= generator.Price;
            generator.Quantity++;
        }
    }

    public PassiveGenerator GetPassiveGeneratorByTag(string tag)
    {
        foreach (PassiveGenerator passiveGenerator in _passiveGenerators)
        {
            if (passiveGenerator.Tag == tag)
                return passiveGenerator;
        }

        return null;
    }

    public bool hasEnough_jobs(BigInteger cost)
    {
        return cost <= _jobs;
    }

    public static string FormatLong(long value)
    {
        switch (value)
        {
            case >= 0 and <= 999:
                return value.ToString(CultureInfo.InvariantCulture);
            case > 999 and <= 999999:
                return value.ToString("0,.#K", CultureInfo.InvariantCulture);
            case > 999999 and <= 999999999:
                return value.ToString("0,.#M", CultureInfo.InvariantCulture);
            case > 999999999 and <= 999999999999:
                return value.ToString("0,.#B", CultureInfo.InvariantCulture);
            case > 999999999999 and <= 999999999999999:
                return value.ToString("0,.#T", CultureInfo.InvariantCulture);
            case > 999999999999999 and <= 999999999999999999:
                return value.ToString("0,.#Qa", CultureInfo.InvariantCulture);
            case > 999999999999999999 and <= long.MaxValue:
                return value.ToString("0,.#Qi", CultureInfo.InvariantCulture);
            default:
                Debug.LogError("Value to format out of range ¿Negative?");
                return "";
        }
    }

}
