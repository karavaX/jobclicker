using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameData _data;

    public void ReincarnateButton() {
        _data.Reincarnate();
    }

    public void exitButton() {
        GameManager.instance.Exit();
    }
}
