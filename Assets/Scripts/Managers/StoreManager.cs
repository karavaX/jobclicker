using UnityEngine;

public class StoreManager : MonoBehaviour
{
    [SerializeField]
    private GameData _data;

    [SerializeField]
    private GameObject _prefab;

    // Start is called before the first frame update
    void Start()
    {
        BuildStore();
    }

    private void BuildStore()
    {
        foreach (PassiveGenerator passiveGenerator in _data.PassiveGenerators)
        {
            // Create GameObject
            GameObject storePassiveGenerator = Instantiate(_prefab, transform);
            
            storePassiveGenerator.name = passiveGenerator.Tag;

            // Get components to modify
            StorePassiveGenerator components = storePassiveGenerator.GetComponent<StorePassiveGenerator>();

            if (components != null)
            {
                // Initialize components
                components.Init(passiveGenerator);
                // Add button action
                components.Button.onClick.AddListener(delegate { BuyPassiveGenerator(passiveGenerator.Tag); });
            }
            else
            {
                Debug.LogError("StorePassiveGenerator prefab does not contain any StorePassiveGenerator component");
            }

        }
    }

    private void BuyPassiveGenerator(string tag)
    {
        _data.BuyPassiveGenerator(tag);

        GameManager.instance.UpdateUI();
    }

}
