using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Singleton stuff
    private static GameManager _instance;

    [SerializeField]
    private GameData _gameData;

    [SerializeField]
    private GameEvent _updateUI;

    private AudioSource _music;

    private bool _musicPlaying = false;

    public static GameManager instance => _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }

        DontDestroyOnLoad(this.gameObject);

        _music = transform.Find("Music").GetComponent<AudioSource>();

        SceneManager.sceneLoaded += OnSceneLoaded;

        ChangeScene("Clicker");
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PassiveSubsystem());
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimer();
    }

    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (!_musicPlaying) {
            _music.Play();

            _musicPlaying = true;
        }

        UpdateUI();
    }


    public void ActiveGeneration()
    {
        this._gameData.activeGeneration();
        UpdateUI();
    }

    IEnumerator PassiveSubsystem()
    {
        while (true)
        {
            yield return new WaitForSeconds(this._gameData.PassiveWaitTime);
            this._gameData.passiveGeneration();
            UpdateUI();
        }

    }

    public void SuitHectorGeneration() {
        _gameData.SuitHectorGeneration();
    }

    private void UpdateTimer()
    {
        float deltaTime = Time.deltaTime;
        this._gameData.PlayTime += deltaTime % 60;
    }

    

    public void UpdateUI()
    {
        this._updateUI.Raise();
    }

    public void Exit()
    {
        // Only in editor!
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }
}
