using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2D : MonoBehaviour
{
    [Header("Aspect ratio")]
    [SerializeField]
    private bool _forceAspectRatio = false;
    [SerializeField]
    private float _ratioX = 16f;
    [SerializeField]
    private float _ratioY = 9f;

    [Header("Camera Lock")]
    [SerializeField]
    private Transform _anchor;
    [SerializeField]
    private bool _lockX;
    [SerializeField]
    private bool _lockY;

    void Start()
    {
        // Aspect ratio
        if (this._forceAspectRatio)
        {
            this.GetComponent<Camera>().aspect = this._ratioX / this._ratioY;
        }
    }

    void Update()
    {
        // Camera Lock
        if (this._lockX || this._lockY)
        {
            float positionX = this._lockX ? this._anchor.position.x : this.transform.position.x;
            float positionY = this._lockY ? this._anchor.position.y : this.transform.position.y;

            this.transform.position = new Vector3(positionX, positionY, this.transform.position.z);
        }
    }
}
